"#####################
" その他
"#####################
set backspace=indent,eol,start

"#####################
"" 表示設定
"#####################
set number "行番号を表示する
set title "編集中のファイル名を表示する
set showmatch "括弧入力時の対応する括弧を表示
syntax on "コードの色分け
set tabstop=4 "インデントをスペース4つ分に設定
set smartindent "オートインデント
set shiftwidth=4 "オートインデント時にインデントする文字数

set list	" タブ、空白、改行の可視化
set listchars=tab:>.,trail:_,eol:←,extends:>,precedes:<,nbsp:%

"全角スペースをハイライト表示
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=reverse ctermfg=DarkMagenta gui=reverse guifg=DarkMagenta
endfunction

if has('syntax')
	augroup ZenkakuSpace
		autocmd!
		autocmd ColorScheme       * call ZenkakuSpace()
		autocmd VimEnter,WinEnter * match ZenkakuSpace /　/
	augroup END
	call ZenkakuSpace()
endif

"au BufRead,BufNewFile *.scss set filetype=sass " Scssをハイライトさせる

"#####################
" 検索設定
"#####################
set ignorecase "大文字/小文字の区別なく検索する
set smartcase " 検索文字列に大文字が含まれている場合は区別する
set wrapscan " 検索時に最後までいったら最初に戻る

"#####################
" Start NeoBundle Setting
"#####################
set nocompatible
filetype plugin indent off

if has('vim_starting')
	set runtimepath+=~/.vim/bundle/neobundle.vim
	call neobundle#begin(expand('~/.vim/bundle/'))
endif

NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/neosnippet.vim'

" NERDTreeを設定
NeoBundle 'scrooloose/nerdtree'

" autoclose
NeoBundle 'Townk/vim-autoclose'

" Emmet
NeoBundle 'mattn/emmet-vim'

" quickrun
NeoBundle 'thinca/vim-quickrun'

" grep.vim
"NeoBundle 'qrep.vim'

" nyan-modoki
NeoBundle 'drillbits/nyan-modoki.vim'

" sass-compile.vim
NeoBundle 'AtsushiM/search-parent.vim'
NeoBundle 'AtsushiM/sass-compile.vim'

call neobundle#end()
NeoBundleCheck
filetype plugin indent on

"######################
" Nyan-modoki Setting
"######################
set laststatus=2
set statusline=%F%m%r%h%w[%{&ff}]%=%{g:NyanModoki()}(%l,%c)[%P]
let g:nyan_modoki_select_cat_face_number=2
let g:nyan_modoki_animation_enabled=1

"#####################
" sass-comile Setting
"#####################
let g:sass_compile_auto=1
let g:sass_compile_cdloop=5
let g:sass_compile_cssdir=['css', 'stylesheet']
let g:sass_compile_file=['scss', 'sass']
let g:sass_started_dirs=[]

let g:sass_compile_beforecmd=""
let g:sass_compile_aftercmd="cp ~/compass_lessons/stylesheets/main.css ~/html/"

autocmd FileType less,sass setlocal sw=2 sts=2 ts=2 et
au! BufWritePost * SassCompile

